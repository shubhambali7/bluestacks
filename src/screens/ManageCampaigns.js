import React, { useState } from "react";
import {
  Header,
  SegmentedButtons,
  Modal,
  PriceDetails,
  CampaignsList,
} from "../components";
import "./manageCampaigns.css";
import useCampaigns from "../services/useCampaigns";
import { useTranslation } from "react-i18next";

const ManageCampaigns = () => {
  const { t } = useTranslation();

  const { campaigns, rescheduleCampaign, isLoading, isError } = useCampaigns();
  const [selectedCampaignType, setSelectedCampaignType] = useState("upcoming");
  const [selectedPriceDetails, setSelectedPriceDetails] = useState(null);

  const CAMPAIGN_TYPES = [
    { id: "upcoming", name: t("upcomingCampaigns") },
    { id: "live", name: t("liveCampaigns") },
    { id: "past", name: t("pastCampaigns") },
  ];

  const onViewPriceDetails = (campaignId) => {
    setSelectedPriceDetails(
      campaigns[selectedCampaignType].find(
        (campaign) => campaign.id === campaignId
      )
    );
  };

  return (
    <div>
      <Header />
      <div className="campaigns__screen">
        <h1>{t("manageCampaigns")}</h1>
        <SegmentedButtons
          selected={selectedCampaignType}
          segments={CAMPAIGN_TYPES}
          onSelect={setSelectedCampaignType}
        />
        <CampaignsList
          data={campaigns[selectedCampaignType]}
          rescheduleCampaign={rescheduleCampaign}
          onViewPriceDetails={onViewPriceDetails}
          isLoading={isLoading}
          isError={isError}
        />
      </div>
      <Modal visible={Boolean(selectedPriceDetails)}>
        <PriceDetails
          campaign={selectedPriceDetails}
          onClosePress={() => setSelectedPriceDetails(null)}
        />
      </Modal>
    </div>
  );
};

export default ManageCampaigns;
