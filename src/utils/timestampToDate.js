function timestampToDate(timestamp) {
  const date = new Date(timestamp)
    .toLocaleDateString("en-GB", {
      day: "numeric",
      month: "short",
      year: "numeric",
    })
    .split(" ");
  return `${date[1]} ${date[2]}, ${date[0]}`;
}

export default timestampToDate;
