import { isSameDay, getDiffDays } from "./index";

function processCampaignData(campaignData) {
  const upcoming = [];
  const live = [];
  const past = [];
  const currentDate = new Date();

  campaignData.forEach((campaign) => {
    const campaignDate = new Date(campaign.createdOn);
    const diffDays = getDiffDays(campaignDate, currentDate);

    if (isSameDay(campaignDate, currentDate)) {
      live.push({ ...campaign, diffDays: 0 });
    } else if (currentDate < campaignDate) {
      upcoming.push({ ...campaign, diffDays });
    } else {
      past.push({ ...campaign, diffDays });
    }
  });

  return {
    upcoming,
    live,
    past,
  };
}

export default processCampaignData;
