function getDiffDays(d1, d2) {
  const diffTime = d1 - d2;
  const diffDays = diffTime / (1000 * 60 * 60 * 24);
  return Math.floor(diffDays);
}

export default getDiffDays;
