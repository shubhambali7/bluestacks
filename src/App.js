import "./App.css";
import { ManageCampaigns } from "./screens";

function App() {
  return (
    <div className="App">
      <ManageCampaigns />
    </div>
  );
}

export default App;
