import React from "react";
import "./priceDetails.css";

const PriceItem = ({ title, price }) => {
  return (
    <div className="price-details__item">
      <span>{title}</span>
      <span>$ {price}</span>
    </div>
  );
};

const PriceDetails = ({ onClosePress, campaign }) => {
  return (
    <div className="price-details">
      <div className="price-details__header">
        <img src={campaign.imageUrl} />
        <div>
          <div className="price-details__name">{campaign.name}</div>
          <div className="price-details__region">{campaign.region}</div>
        </div>
      </div>
      <div className="price-details__content">
        <span className="price-details__content-heading">Pricing</span>
        <PriceItem title="1 Week - 1 Month" price="100.00" />
        <PriceItem title="6 Months" price="500.00" />
        <PriceItem title="1 Year" price="900.00" />
      </div>
      <button onClick={onClosePress}>Close</button>
    </div>
  );
};

export default PriceDetails;
