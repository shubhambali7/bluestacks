import React, { useLayoutEffect } from "react";
import ReactDOM from "react-dom";
import "./modal.css";

const Modal = ({ visible, children }) => {
  useLayoutEffect(() => {
    if (visible) {
      document.body.classList.add("modal-open");
    } else {
      document.body.classList.remove("modal-open");
    }
  }, [visible]);
  if (!visible) return null;
  return ReactDOM.createPortal(
    <div className="modal">
      <div className="modal__content">{children}</div>
    </div>,
    document.querySelector("#modal")
  );
};

export default Modal;
