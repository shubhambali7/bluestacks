import React from "react";
import "./iconButton.css";

const IconButton = ({ icon, label, children, onClick }) => {
  return (
    <div onClick={onClick} className="icon-button">
      <img src={icon} />
      {children || <span>{label}</span>}
    </div>
  );
};

export default IconButton;
