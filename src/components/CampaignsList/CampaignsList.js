import React from "react";
import "./campaignsList.css";
import CampaignItem from "./CampaignItem";
import { useTranslation } from "react-i18next";

function useMessage({ data, isLoading, isError }) {
  const { t } = useTranslation();

  if (isLoading) return t("fetching");
  if (isError) return t("error");
  if (data.length === 0) return t("empty");
}

const CampaignsList = (props) => {
  const { t } = useTranslation();
  const message = useMessage({
    data: props.data,
    isLoading: props.isLoading,
    isError: props.isError,
  });
  return (
    <div className="campaigns__list">
      <div className="campaigns__list-header campaigns__list-row">
        <div>{t("date")}</div>
        <div>{t("view")}</div>
        <div>{t("campaign")}</div>
        <div>{t("actions")}</div>
      </div>
      {message && <div className="campaigns__message">{message}</div>}
      {props.data.map((campaign) => (
        <CampaignItem
          key={campaign.id}
          {...campaign}
          rescheduleCampaign={props.rescheduleCampaign}
          onViewPriceDetails={props.onViewPriceDetails}
        />
      ))}
    </div>
  );
};

export default CampaignsList;
