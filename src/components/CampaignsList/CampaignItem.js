import React, { useMemo } from "react";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import { IconButton } from "../index";
import "./campaignsList.css";
import { timestampToDate } from "../../utils";
import { useTranslation } from "react-i18next";

const CampaignTitle = ({ name, region, imageUrl }) => (
  <div className="campaign__title">
    <img src={imageUrl} />
    <div>
      <div className="campaign__title-name">{name}</div>
      <div className="campaign__title-region">{region}</div>
    </div>
  </div>
);

const CampaignPricing = ({ price, id, onViewPriceDetails }) => {
  const { t } = useTranslation();

  return (
    <div className="campaigns__pricing">
      <IconButton
        icon="assets/price.png"
        label={t("viewPricing")}
        onClick={() => onViewPriceDetails(id)}
      />
    </div>
  );
};

const CampaignDate = ({ createdOn, diffDays }) => {
  const { t } = useTranslation();

  let diffLabel = t("today");
  if (diffDays < 0) {
    diffLabel = `${Math.abs(diffDays)} ${
      diffDays < -1 ? t("daysAgo") : t("dayAgo")
    }`;
  }
  if (diffDays > 0) {
    diffLabel = `${diffDays} ${diffDays > 1 ? t("daysAhead") : t("dayAhead")}`;
  }

  const dateLabel = useMemo(() => timestampToDate(createdOn), [createdOn]);

  return (
    <div>
      <div className="campaign__date--normal">{dateLabel}</div>
      <div className="campaign__date--italic">{diffLabel}</div>
    </div>
  );
};

const CampaignActions = ({
  csv,
  report,
  rescheduleCampaign,
  id,
  createdOn,
}) => {
  const { t } = useTranslation();

  return (
    <div className="campaigns__actions">
      <IconButton icon="assets/csv.png" label={t("csv")} />
      <IconButton icon="assets/report.png" label={t("report")} />
      <DatePicker
        selected={createdOn}
        onChange={(date) => rescheduleCampaign(id, date)}
        customInput={
          <IconButton icon="assets/calendar.png">
            <span>{t("scheduleAgain")}</span>
          </IconButton>
        }
      />
    </div>
  );
};

const CampaignItem = ({
  id,
  name,
  region,
  createdOn,
  diffDays,
  price,
  csv,
  report,
  imageUrl,
  rescheduleCampaign,
  onViewPriceDetails,
}) => {
  return (
    <div className="campaigns__list-row">
      <CampaignDate createdOn={createdOn} diffDays={diffDays} />
      <CampaignTitle name={name} region={region} imageUrl={imageUrl} />
      <CampaignPricing
        id={id}
        price={price}
        onViewPriceDetails={onViewPriceDetails}
      />
      <CampaignActions
        id={id}
        csv={csv}
        report={report}
        createdOn={createdOn}
        rescheduleCampaign={rescheduleCampaign}
      />
    </div>
  );
};

export default CampaignItem;
