import React from "react";
import "./segmentedButtons.css";

const Segment = ({ id, name, onSelect, selected }) => {
  return (
    <div
      className={
        selected === id
          ? "segmented-buttons__item--active"
          : "segmented-buttons__item"
      }
      onClick={() => onSelect(id)}
    >
      {name}
    </div>
  );
};

const SegmentedButtons = ({
  segments = [],
  onSelect,
  selected = segments[0]?.key,
}) => {
  return (
    <div className="segmented-buttons__container">
      {segments.map((segment) => (
        <Segment
          key={segment.id}
          {...segment}
          onSelect={onSelect}
          selected={selected}
        />
      ))}
    </div>
  );
};

export default SegmentedButtons;
