import React, { useState, useEffect } from "react";
import "./header.css";
import i18n from "i18next";

const Header = () => {
  const [language, setLanguage] = useState("en");

  useEffect(() => {
    i18n.changeLanguage(language);
  }, [language]);

  const onChangeLangugaeClick = () => {
    setLanguage((prevLang) => {
      if (prevLang === "es") return "en";
      if (prevLang === "en") return "es";
    });
  };

  return (
    <div className="app__header">
      <img src="/assets/logo.png" />
      <div onClick={onChangeLangugaeClick}>
        Change Language: {language === "es" ? "EN" : "ES"}
      </div>
    </div>
  );
};

export default Header;
