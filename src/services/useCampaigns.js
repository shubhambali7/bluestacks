import useSWR, { mutate } from "swr";
import { useMemo } from "react";
import { processCampaignData } from "../utils";

const useCampaigns = () => {
  const URL = "https://api.jsonbin.io/b/6069e1098be464182c588242/2";
  const { data: { data = [] } = {}, error } = useSWR(URL);

  const campaigns = useMemo(() => processCampaignData(data), [data]);

  function rescheduleCampaign(campaignId, rescheduleDate) {
    mutate(
      URL,
      {
        data: data.map((campaign) =>
          campaignId === campaign.id
            ? { ...campaign, createdOn: rescheduleDate }
            : campaign
        ),
      },
      false
    );
  }

  return {
    campaigns,
    rescheduleCampaign,
    isLoading: !error && !data,
    isError: error,
  };
};

export default useCampaigns;
